package com.dianapps.dictionaryapp.Adapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.dianapps.dictionaryapp.Activity.WordDetailsActivity;
import com.dianapps.dictionaryapp.R;
import com.dianapps.dictionaryapp.databinding.ExampleLayoutBinding;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.VHolder> {

    List<String> list;
    Context context;

    public HistoryAdapter(List<String> list,Context context) {

        this.list = list;
        this.context=context;
    }

    @NonNull
    @Override
    public HistoryAdapter.VHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ExampleLayoutBinding binding = DataBindingUtil.inflate(inflater, R.layout.example_layout, parent, false);
        return new HistoryAdapter.VHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryAdapter.VHolder holder, @SuppressLint("RecyclerView") int position) {
        ExampleLayoutBinding binding = (ExampleLayoutBinding) holder.getBinding();
        binding.layoutText.setText("\n \u25CF   " + list.get(position));

        binding.layoutText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, WordDetailsActivity.class);
                intent.putExtra("word", list.get(position));
                context.startActivities(new Intent[]{intent});
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VHolder extends RecyclerView.ViewHolder {
        ExampleLayoutBinding binding;

        public VHolder(@NonNull ExampleLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public ExampleLayoutBinding getBinding() {
            return binding;
        }
    }
}
