package com.dianapps.dictionaryapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.dianapps.dictionaryapp.Activity.WordDetailsActivity;
import com.dianapps.dictionaryapp.R;
import com.dianapps.dictionaryapp.databinding.WordListLayoutBinding;

import java.util.List;
import java.util.Random;

public class WordListAdapter extends RecyclerView.Adapter<WordListAdapter.VHolder> {

    List<String> list;
    Context context;

    public WordListAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;
    }
    @NonNull
    @Override
    public VHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        WordListLayoutBinding binding = DataBindingUtil.inflate(inflater, R.layout.word_list_layout, parent, false);
        return new VHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull VHolder holder, @SuppressLint("RecyclerView") int position) {
        WordListLayoutBinding binding = (WordListLayoutBinding) holder.getBinding();
        binding.layoutText.setText(list.get(position));
        binding.tvFirst.setText(list.get(position).substring(0, 1));

        Random mRandom = new Random();
        int color = Color.argb(155, mRandom.nextInt(156), mRandom.nextInt(156), mRandom.nextInt(166));
        ((GradientDrawable) binding.tvFirst.getBackground()).setColor(color);

        binding.layoutText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, WordDetailsActivity.class);
                intent.putExtra("word", list.get(position));
                context.startActivities(new Intent[]{intent});
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VHolder extends RecyclerView.ViewHolder {
        WordListLayoutBinding binding;

        public VHolder(@NonNull WordListLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public WordListLayoutBinding getBinding() {
            return binding;
        }
    }
}
