package com.dianapps.dictionaryapp.Adapter;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.dianapps.dictionaryapp.R;
import com.dianapps.dictionaryapp.databinding.ExampleLayoutBinding;
import java.util.List;

public class ExampleAdapter extends RecyclerView.Adapter<ExampleAdapter.VHolder> {

    List<String> list;

    public ExampleAdapter(List<String> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ExampleAdapter.VHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ExampleLayoutBinding binding = DataBindingUtil.inflate(inflater, R.layout.example_layout, parent, false);
        return new ExampleAdapter.VHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ExampleAdapter.VHolder holder, int position) {
        ExampleLayoutBinding binding = (ExampleLayoutBinding) holder.getBinding();
        binding.layoutText.setText("\n \u25CF   " + list.get(position));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VHolder extends RecyclerView.ViewHolder {
        ExampleLayoutBinding binding;

        public VHolder(@NonNull ExampleLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public ExampleLayoutBinding getBinding() {
            return binding;
        }
    }
}
