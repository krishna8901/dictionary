package com.dianapps.dictionaryapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.dianapps.dictionaryapp.Activity.WordDetailsActivity;
import com.dianapps.dictionaryapp.R;
import com.dianapps.dictionaryapp.databinding.SearchLayoutBinding;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViHolder> implements Filterable {

    List<String> searchList;
    List<String> backup;
    Context context;
    onClick onClick;

    public SearchAdapter(Context context, List<String> searchList, onClick onClick) {
        this.searchList = searchList;
        this.context = context;
        this.onClick = onClick;
        backup = new ArrayList<>(searchList);
    }

    @NonNull
    @Override
    public ViHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        SearchLayoutBinding binding = DataBindingUtil.inflate(inflater, R.layout.search_layout, parent, false);
        return new ViHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViHolder holder, @SuppressLint("RecyclerView") int position) {

        SearchLayoutBinding binding = (SearchLayoutBinding) holder.getBinding();
        binding.layoutText.setText(searchList.get(position));
        binding.layoutText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.OnClick(searchList.get(position));
                Intent intent = new Intent(context, WordDetailsActivity.class);
                intent.putExtra("word", searchList.get(position));
                context.startActivities(new Intent[]{intent});
            }
        });
    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence keyword) {
            List<String> filteredData = new ArrayList<>();
            if (keyword.toString().isEmpty())
                filteredData.addAll(backup);
            else {
                for (String obj : backup) {
                    if (obj != null && (obj.toLowerCase().contains(keyword.toString().toLowerCase()))) {
                        filteredData.add(obj);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredData;
            return results;
        }

        @SuppressLint("NotifyDataSetChanged")
        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

            searchList.clear();
            searchList.addAll((List<String>) filterResults.values);
            notifyDataSetChanged();
        }
    };

    public static class ViHolder extends RecyclerView.ViewHolder {
        SearchLayoutBinding binding;

        public ViHolder(@NonNull SearchLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public SearchLayoutBinding getBinding() {
            return binding;
        }
    }

    public interface onClick {
        public void OnClick(String data);
    }
}
