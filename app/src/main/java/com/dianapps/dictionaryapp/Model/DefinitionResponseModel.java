package com.dianapps.dictionaryapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DefinitionResponseModel {

    @SerializedName("partOfSpeech")
    @Expose
    private String partOfSpeech;
    @SerializedName("attributionText")
    @Expose
    private String attributionText;
    @SerializedName("sourceDictionary")
    @Expose
    private String sourceDictionary;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("labels")
    @Expose
    private List<Object> labels = null;
    @SerializedName("citations")
    @Expose
    private List<Object> citations = null;
    @SerializedName("word")
    @Expose
    private String word;
    @SerializedName("relatedWords")
    @Expose
    private List<Object> relatedWords = null;
    @SerializedName("exampleUses")
    @Expose
    private List<Object> exampleUses = null;
    @SerializedName("textProns")
    @Expose
    private List<Object> textProns = null;
    @SerializedName("notes")
    @Expose
    private List<Object> notes = null;
    @SerializedName("attributionUrl")
    @Expose
    private String attributionUrl;
    @SerializedName("wordnikUrl")
    @Expose
    private String wordnikUrl;

    public String getPartOfSpeech() {
        return partOfSpeech;
    }

    public void setPartOfSpeech(String partOfSpeech) {
        this.partOfSpeech = partOfSpeech;
    }

    public String getAttributionText() {
        return attributionText;
    }

    public void setAttributionText(String attributionText) {
        this.attributionText = attributionText;
    }

    public String getSourceDictionary() {
        return sourceDictionary;
    }

    public void setSourceDictionary(String sourceDictionary) {
        this.sourceDictionary = sourceDictionary;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Object> getLabels() {
        return labels;
    }

    public void setLabels(List<Object> labels) {
        this.labels = labels;
    }

    public List<Object> getCitations() {
        return citations;
    }

    public void setCitations(List<Object> citations) {
        this.citations = citations;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public List<Object> getRelatedWords() {
        return relatedWords;
    }

    public void setRelatedWords(List<Object> relatedWords) {
        this.relatedWords = relatedWords;
    }

    public List<Object> getExampleUses() {
        return exampleUses;
    }

    public void setExampleUses(List<Object> exampleUses) {
        this.exampleUses = exampleUses;
    }

    public List<Object> getTextProns() {
        return textProns;
    }

    public void setTextProns(List<Object> textProns) {
        this.textProns = textProns;
    }

    public List<Object> getNotes() {
        return notes;
    }

    public void setNotes(List<Object> notes) {
        this.notes = notes;
    }

    public String getAttributionUrl() {
        return attributionUrl;
    }

    public void setAttributionUrl(String attributionUrl) {
        this.attributionUrl = attributionUrl;
    }

    public String getWordnikUrl() {
        return wordnikUrl;
    }

    public void setWordnikUrl(String wordnikUrl) {
        this.wordnikUrl = wordnikUrl;
    }
}
