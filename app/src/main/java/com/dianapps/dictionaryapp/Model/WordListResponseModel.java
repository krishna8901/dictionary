package com.dianapps.dictionaryapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WordListResponseModel {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("word")
        @Expose
        private String word;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getWord() {
            return word;
        }

        public void setWord(String word) {
            this.word = word;
        }

    }
