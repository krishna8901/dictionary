package com.dianapps.dictionaryapp.Model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class ExampleResponseModel {

    @SerializedName("examples")
    @Expose
    private List<DailyWordResponseModel.Example__1> examples = null;

    public List<DailyWordResponseModel.Example__1> getExamples() {
        return examples;
    }
    public void setExamples(List<DailyWordResponseModel.Example__1> examples) {
        this.examples = examples;
    }

    public class Example__1 {

        @SerializedName("provider")
        @Expose
        private Provider provider;
        @SerializedName("year")
        @Expose
        private Integer year;
        @SerializedName("rating")
        @Expose
        private Double rating;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("word")
        @Expose
        private String word;
        @SerializedName("text")
        @Expose
        private String text;
        @SerializedName("documentId")
        @Expose
        private Integer documentId;
        @SerializedName("exampleId")
        @Expose
        private Integer exampleId;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("author")
        @Expose
        private String author;

        public Provider getProvider() {
            return provider;
        }

        public void setProvider(Provider provider) {
            this.provider = provider;
        }

        public Integer getYear() {
            return year;
        }

        public void setYear(Integer year) {
            this.year = year;
        }

        public Double getRating() {
            return rating;
        }

        public void setRating(Double rating) {
            this.rating = rating;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getWord() {
            return word;
        }

        public void setWord(String word) {
            this.word = word;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public Integer getDocumentId() {
            return documentId;
        }

        public void setDocumentId(Integer documentId) {
            this.documentId = documentId;
        }

        public Integer getExampleId() {
            return exampleId;
        }

        public void setExampleId(Integer exampleId) {
            this.exampleId = exampleId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

    }

    public class Provider {

        @SerializedName("id")
        @Expose
        private Integer id;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }
    }
}
