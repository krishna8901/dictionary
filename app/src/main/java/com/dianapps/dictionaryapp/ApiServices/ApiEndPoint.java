package com.dianapps.dictionaryapp.ApiServices;

import com.dianapps.dictionaryapp.Model.DailyWordResponseModel;
import com.dianapps.dictionaryapp.Model.DefinitionResponseModel;
import com.dianapps.dictionaryapp.Model.ExampleResponseModel;
import com.dianapps.dictionaryapp.Model.SearchListResponseModel;
import com.dianapps.dictionaryapp.Model.WordListResponseModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiEndPoint {

    @GET("v4/words.json/wordOfTheDay")
    Call<DailyWordResponseModel> getDailyWord(@Query("date") String date, @Query("api_key") String api_key);

    @GET("v4/words.json/randomWords")
    Call<List<WordListResponseModel>> getRandomWord(@Query("word") String word, @Query("api_key") String api_key, @Query("limit") int itemLimit);

    @GET("v4/words.json/randomWords")
    Call<List<SearchListResponseModel>> getRandomSearchWord(@Query("word") String word, @Query("api_key") String api_key);

    @GET
    Call<List<DefinitionResponseModel>> getDefinition(@Url String url, @Query("api_key") String api_key);

//    @GET("v4/word.json/compelimur/definitions")
//    Call<DefinitionResponseModel> getDefinition(@Query("definition") String definition, @Query("api_key") String api_key);

    @GET
    Call<ExampleResponseModel> getExample(@Url String url, @Query("api_key") String api_key);

//    @GET("v4/word.json/compelimur/examples")
//    Call<ExampleResponseModel> getExamples(@Query("word") String examples, @Query("api_key") String api_key);

}