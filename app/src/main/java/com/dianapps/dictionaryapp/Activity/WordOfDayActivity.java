package com.dianapps.dictionaryapp.Activity;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.Toast;

import com.dianapps.dictionaryapp.Adapter.ExampleAdapter;
import com.dianapps.dictionaryapp.Model.DailyWordResponseModel;
import com.dianapps.dictionaryapp.R;
import com.dianapps.dictionaryapp.databinding.ActivityWordOfDayBinding;
import com.orhanobut.hawk.Hawk;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class WordOfDayActivity extends AppCompatActivity {

    ActivityWordOfDayBinding binding;
    DailyWordResponseModel dailyWord;
    TextToSpeech tts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_word_of_day);
        binding.back.setOnClickListener(view -> onBackPressed());
        Hawk.init(this).build();

        dailyWord = Hawk.get("dailyWordResponse");
        binding.words.setText(dailyWord.getWord());
        Hawk.put("textWord", dailyWord.getWord());

        List<String> allList;

        try{
            allList=Hawk.get("text");
            if(allList.contains(dailyWord.getWord())) {
                binding.savedHide.setVisibility(View.VISIBLE);
            }
        }catch (NullPointerException e){
        }

        binding.definitionRv.setLayoutManager(new LinearLayoutManager(getApplication()));
        ArrayList<String> definition = new ArrayList<String>();
        for (int i = 0; i < dailyWord.getDefinitions().size(); i++)
            definition.add(dailyWord.getDefinitions().get(i).getText());
        binding.definitionRv.setAdapter(new ExampleAdapter(definition));


        binding.exampleRv.setLayoutManager(new LinearLayoutManager(getApplication()));
        ArrayList<String> example = new ArrayList<String>();
        for (int i = 0; i < dailyWord.getExamples().size(); i++)
            example.add(dailyWord.getExamples().get(i).getText());
        binding.exampleRv.setAdapter(new ExampleAdapter(example));


        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i != TextToSpeech.ERROR) {
                    tts.setLanguage(Locale.ENGLISH);
                }
            }
        });

        binding.volume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tts.isSpeaking()) {
                    speaker("");
                    tts.stop();
                } else {
                    speaker("speak");
                }
            }
        });

        binding.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharetextOnly("Word- " + dailyWord.getWord().toString() + "\n\nDefinition- " + dailyWord.getDefinitions().get(0).getText());
            }

        });

        binding.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Hawk.get("login").equals("Google")) {

                List<String> data;

                if(Hawk.get("text") != null){
                    data = Hawk.get("text");
                    data.add(dailyWord.getWord());

                    if(binding.savedHide.getVisibility() != View.VISIBLE) {
                        Hawk.put("text", data);
                    }
                }else{
                    data = new ArrayList<String>();
                    data.add(dailyWord.getWord());
                    if(binding.savedHide.getVisibility() != View.VISIBLE) {
                        Hawk.put("text", data);
                    }
                }
                if (binding.savedHide.getVisibility() != View.VISIBLE) {
                    binding.savedHide.setVisibility(View.VISIBLE);
                } else {
                    data = new ArrayList<String>();
                    data = Hawk.get("text");
                    data.remove(dailyWord.getWord());

                    if(binding.savedHide.getVisibility() == View.VISIBLE) {
                        Hawk.put("text", data);
                    }
                    binding.savedHide.setVisibility(View.GONE);
                }
            } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(WordOfDayActivity.this);
                    builder.setMessage("Please Login First For Save Word!")
                            .setCancelable(false)
                            .setPositiveButton("Login", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(WordOfDayActivity.this,LoginActivity.class);
                                    startActivity(intent);
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.setTitle("Save Word");
                    alert.show();
                }
            }
        });
    }

    public void speaker(String s) {
        tts.speak(dailyWord.getWord() + dailyWord.getDefinitions().get(0).getText() + dailyWord.getExamples().get(0).getText(), TextToSpeech.QUEUE_FLUSH, null);
        if (s.equals("speak")) {
            binding.volume.setBackgroundResource(R.drawable.volume_background);
        } else {
            binding.volume.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onBackPressed() {
        tts.shutdown();
        super.onBackPressed();
    }

    public void sharetextOnly(String title) {
        String sharebody = title;
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, sharebody);
        startActivity(Intent.createChooser(intent, "Share Via"));
    }

}