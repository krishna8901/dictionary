package com.dianapps.dictionaryapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CalendarView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.dianapps.dictionaryapp.ApiServices.ApiEndPoint;
import com.dianapps.dictionaryapp.ApiServices.ServiceGenerator;
import com.dianapps.dictionaryapp.Model.DailyWordResponseModel;
import com.dianapps.dictionaryapp.R;
import com.dianapps.dictionaryapp.databinding.ActivityWordOfDayCalenderBinding;
import com.orhanobut.hawk.Hawk;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WordOfDayCalender extends AppCompatActivity {

    ActivityWordOfDayCalenderBinding binding;
    Call<DailyWordResponseModel> call;
    ApiEndPoint apiEndPoint;
    String date, stringDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_word_of_day_calender);
        calenderApi();
        binding.back.setOnClickListener(view -> onBackPressed());

        binding.calender.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {

                i1 = i1 + 1;
                date = i + "/" + i1 + "/" + i2;
                Log.e("time", "onSelectedDayChange: " + date);
                Date date1;
                Calendar calendar = Calendar.getInstance();
                Date today = calendar.getTime();

                try {
                    date1 = new SimpleDateFormat("yyyy/MM/dd").parse(date);
                    SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
                    stringDate = formatDate.format(date1);

                    Log.e("time1", "onSelectedDayChange: " + date1);
                    if (date1.before(today) || date1.equals(today)) {
                        calenderApi();

                    } else {
                        calendarView.setMaxDate(today.getTime());
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void calenderApi() {
        Hawk.init(this).build();
        apiEndPoint = ServiceGenerator.createService(ApiEndPoint.class);
        String accessToken = "q095std75p9uysknwxmaxeyw0zqzsaa0spj8jln0rm9tiv653";

        call = apiEndPoint.getDailyWord(stringDate, accessToken);
        Log.e("API", "calenderApi: " + stringDate);
        call.enqueue(new Callback<DailyWordResponseModel>() {
            @Override
            public void onResponse(Call<DailyWordResponseModel> call, Response<DailyWordResponseModel> response) {
                if (response.isSuccessful()) {
                    binding.includeProgressbar.setVisibility(View.GONE);
                    response.body().getWord();
                    Hawk.put("dailyWordResponse", response.body());
                    binding.dayWord.setText(response.body().getWord());
                    binding.dayWord.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(WordOfDayCalender.this, WordOfDayActivity.class);
                            startActivity(intent);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<DailyWordResponseModel> call, Throwable t) {

            }
        });
    }
}