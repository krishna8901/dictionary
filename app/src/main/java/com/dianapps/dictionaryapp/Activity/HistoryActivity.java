package com.dianapps.dictionaryapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.Toast;

import com.dianapps.dictionaryapp.Adapter.HistoryAdapter;
import com.dianapps.dictionaryapp.R;
import com.dianapps.dictionaryapp.databinding.ActivityHistoryBinding;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

public class HistoryActivity extends AppCompatActivity {

    HistoryAdapter adapter;
    ActivityHistoryBinding binding;
    @SuppressLint("NotifyDataSetChanged")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         binding = DataBindingUtil.setContentView(this, R.layout.activity_history);
        binding.back.setOnClickListener(view -> onBackPressed());
        Hawk.init(this).build();

        binding.historyRv.setLayoutManager(new LinearLayoutManager(this));

        if (Hawk.get("history") != null) {
            adapter = new HistoryAdapter(Hawk.get("history"), HistoryActivity.this);
        } else {
            Toast.makeText(this, "No History found!", Toast.LENGTH_SHORT).show();
        }
        binding.historyRv.setAdapter(adapter);
    }
}