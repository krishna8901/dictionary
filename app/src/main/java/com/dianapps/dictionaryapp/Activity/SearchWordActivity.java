package com.dianapps.dictionaryapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.View;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.dianapps.dictionaryapp.Adapter.SearchAdapter;
import com.dianapps.dictionaryapp.ApiServices.ApiEndPoint;
import com.dianapps.dictionaryapp.Model.SearchListResponseModel;
import com.dianapps.dictionaryapp.R;
import com.dianapps.dictionaryapp.ApiServices.ServiceGenerator;
import com.dianapps.dictionaryapp.databinding.ActivitySearchWordBinding;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchWordActivity extends AppCompatActivity implements Filterable {
    ApiEndPoint apiEndPoint;
    Call<List<SearchListResponseModel>> call;
    ActivitySearchWordBinding binding;
    SearchAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Hawk.init(this).build();
        SearchWordApi();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_word);
        binding.back.setOnClickListener(view -> onBackPressed());

        binding.etSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
               List<String> ll= Hawk.get(HawkKeys.headersKey);

                if ( ll.contains(query)){
                    Toast.makeText(SearchWordActivity.this, "Data found!", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(SearchWordActivity.this, "not found!", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                try {
                    adapter.getFilter().filter(newText);
                    return true;

                }catch (NullPointerException e){
                }
                return false;
            }
        });
    }
    public void SearchWordApi() {
        Hawk.init(this).build();
        apiEndPoint = ServiceGenerator.createService(ApiEndPoint.class);
        String accessToken = "q095std75p9uysknwxmaxeyw0zqzsaa0spj8jln0rm9tiv653";
        String date = "20220-03-03";

        call = apiEndPoint.getRandomSearchWord(date, accessToken);
        call.enqueue(new Callback<List<SearchListResponseModel>>() {
            @Override
            public void onResponse(Call<List<SearchListResponseModel>> call, Response<List<SearchListResponseModel>> response) {
                binding.progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    binding.includeProgressbar.setVisibility(View.GONE);

                    binding.rvSearch.setLayoutManager(new LinearLayoutManager(getApplication()));
                    List<String> word = new ArrayList<>();
                    for (int i = 0; i < response.body().size(); i++) {
                        word.add(response.body().get(i).getWord());
                    }
                    Hawk.put(HawkKeys.headersKey,word);
                    adapter = new SearchAdapter(SearchWordActivity.this, word, new SearchAdapter.onClick() {
                        @Override
                        public void OnClick(String data) {
                            List<String> data1;
                            if (Hawk.get("history") != null) {
                                data1 = Hawk.get("history");

                            } else {
                                data1 = new ArrayList<>();
                            }
                            data1.add(data);
                            Hawk.put("history", data1);
                        }
                    });
                    binding.rvSearch.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<SearchListResponseModel>> call, Throwable t) {

            }
        });
    }

    @Override
    public Filter getFilter() {
        return null;
    }
}