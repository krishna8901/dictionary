package com.dianapps.dictionaryapp.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dianapps.dictionaryapp.ApiServices.ApiEndPoint;
import com.dianapps.dictionaryapp.Model.DailyWordResponseModel;
import com.dianapps.dictionaryapp.Model.WordListResponseModel;
import com.dianapps.dictionaryapp.R;
import com.dianapps.dictionaryapp.ApiServices.ServiceGenerator;
import com.dianapps.dictionaryapp.Adapter.WordListAdapter;
import com.dianapps.dictionaryapp.databinding.ActivityHomeBinding;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.material.navigation.NavigationView;
import com.orhanobut.hawk.Hawk;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    ActivityHomeBinding binding;
    ApiEndPoint apiEndPoint;
    Call<DailyWordResponseModel> call;
    Call<List<WordListResponseModel>> wordlistCall;

    List<WordListResponseModel> worldresponselist;

    List<String> list = new ArrayList<>();
    GoogleSignInOptions gso;
    TextView navUsername;
    GoogleSignInClient mGoogleSignInClient;

    int page = 1, limit = 38;
    LinearLayoutManager manager = new LinearLayoutManager(this);
    WordListAdapter adapter;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        DailyWord();
        RandomWord(page, limit);
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null) {
            String personName = account.getDisplayName();
            Uri personImage = account.getPhotoUrl();
            binding.tool.adminName.setText(personName);
            View headerView = binding.navigate.getHeaderView(0);
            navUsername = headerView.findViewById(R.id.admin);
            ImageView image = headerView.findViewById(R.id.person);
            Picasso.with(getBaseContext()).load(personImage).into(image);
            navUsername.setText(personName);
        }

        binding.nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
                    page++;
                    RandomWord(page, limit);
                }
            }
        });

        binding.tool.menu.setOnClickListener(view -> {
            if (binding.myDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            } else {
                binding.myDrawerLayout.openDrawer(GravityCompat.START);
            }
        });

        binding.navigate.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @SuppressLint("NonConstantResourceId")
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.save:
                        Intent in = new Intent(HomeActivity.this, SaveWordActivity.class);
                        startActivity(in);
                        break;

                    case R.id.word:
                        Intent inn = new Intent(HomeActivity.this, WordOfDayCalender.class);
                        startActivity(inn);
                        break;

                    case R.id.history:
                        Intent intet = new Intent(HomeActivity.this, HistoryActivity.class);
                        startActivity(intet);
                        break;

                    case R.id.settings:
                        Intent intent = new Intent(HomeActivity.this, SettingsActivity.class);
                        if (navUsername == null) {
                            intent.putExtra("login", "Login");
                        } else {
                            intent.putExtra("login", "Logout");
                        }
                        startActivity(intent);

                        break;
                }
                return false;
            }
        });


        binding.tool.search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, SearchWordActivity.class);
                startActivity(intent);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void DailyWord() {
        Hawk.init(this).build();
        apiEndPoint = ServiceGenerator.createService(ApiEndPoint.class);
        String accessToken = "q095std75p9uysknwxmaxeyw0zqzsaa0spj8jln0rm9tiv653";
        Hawk.put("token", accessToken);

        //   String accessToken = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy0-MM-dd", Locale.getDefault());
        String stringDate = formatDate.format(today);

        call = apiEndPoint.getDailyWord(stringDate, accessToken);
        call.enqueue(new Callback<DailyWordResponseModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<DailyWordResponseModel> call, Response<DailyWordResponseModel> response) {
                if (response.isSuccessful()) {
                    Hawk.put("token", accessToken);
                    Hawk.put("dailyWordResponse", response.body());
                    binding.word.setText(Html.fromHtml(response.body().getWord()));
                    binding.date.setText(response.body().getPdd());
                    binding.abc.setVisibility(View.VISIBLE);

                    binding.word.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(HomeActivity.this, WordOfDayActivity.class);
                            startActivity(intent);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<DailyWordResponseModel> call, Throwable t) {

            }
        });
    }

    public void RandomWord(int page, int limit) {
        apiEndPoint = ServiceGenerator.createService(ApiEndPoint.class);
        String accessToken = "q095std75p9uysknwxmaxeyw0zqzsaa0spj8jln0rm9tiv653";
        String word = "xyz";
        wordlistCall = apiEndPoint.getRandomWord(word, accessToken, limit);
        wordlistCall.enqueue(new Callback<List<WordListResponseModel>>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(Call<List<WordListResponseModel>> call, Response<List<WordListResponseModel>> response) {
                if (response.isSuccessful()) {
                    binding.includeProgressbar.setVisibility(View.GONE);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    worldresponselist=response.body();
                    handleresponse(response.body());

//                    List<String> wordlist = new ArrayList<>();
//                    for (int i = 0; i < response.body().size(); i++)
//                        wordlist.add(String.valueOf(Html.fromHtml(response.body().get(i).getWord())));
//
//                    adapter = new WordListAdapter(HomeActivity.this, wordlist);
//                    binding.wordListRecyclerview.setAdapter(adapter);
//                    binding.wordListRecyclerview.setLayoutManager(manager);
//                    adapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onFailure(Call<List<WordListResponseModel>> call, Throwable t) {
            }
        });
    }


    private void handleresponse(List<WordListResponseModel> body)
    {
        worldresponselist=body;

        if(!worldresponselist.isEmpty()){
            List<String> wordlist = new ArrayList<>();
            for (int i = 0; i < worldresponselist.size(); i++)
                wordlist.add(String.valueOf(Html.fromHtml(worldresponselist.get(i).getWord())));

            adapter = new WordListAdapter(HomeActivity.this, wordlist);
            binding.wordListRecyclerview.setAdapter(adapter);
            binding.wordListRecyclerview.setLayoutManager(manager);
            adapter.notifyDataSetChanged();
        }else {
            Toast.makeText(HomeActivity.this, body.toString() ,Toast.LENGTH_SHORT).show();

        }

    }
}