package com.dianapps.dictionaryapp.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.view.View;
import android.widget.Toast;

import com.dianapps.dictionaryapp.ApiServices.ApiEndPoint;
import com.dianapps.dictionaryapp.Adapter.ExampleAdapter;
import com.dianapps.dictionaryapp.Model.DefinitionResponseModel;
import com.dianapps.dictionaryapp.Model.ExampleResponseModel;
import com.dianapps.dictionaryapp.R;
import com.dianapps.dictionaryapp.ApiServices.ServiceGenerator;
import com.dianapps.dictionaryapp.databinding.ActivityWordDetailsBinding;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WordDetailsActivity extends AppCompatActivity {

    ApiEndPoint apiEndPoint;
    Call<List<DefinitionResponseModel>> definitionResponseModels;
    Call<ExampleResponseModel> exampleResponseModels;
    ActivityWordDetailsBinding binding;
    TextToSpeech tts;
    String finalResponseData = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_word_details);
        binding.back.setOnClickListener(view -> onBackPressed());
        Hawk.init(this).build();

        definitionApi();
        exampleApi();
        String wrd = getIntent().getStringExtra("word");
        binding.words.setText(wrd);
        finalResponseData = finalResponseData + wrd;

        List<String> allList;

        allList=Hawk.get("text");

        if(allList!=null &&  allList.contains(wrd)) {
            binding.savedHide.setVisibility(View.VISIBLE);
        }

        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i != TextToSpeech.ERROR) {
                    tts.setLanguage(Locale.ENGLISH);
                }
            }
        });

        binding.volume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tts.isSpeaking()) {
                    speaker("", wrd);
                    tts.stop();
                } else {
                    speaker("speak", wrd);
                }
            }
        });

        binding.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Hawk.get("login").equals("Google")) {

                    List<String> data;
                    if (Hawk.get("text") != null) {

                        data = Hawk.get("text");
                        data.add(wrd);
                        if(binding.savedHide.getVisibility() != View.VISIBLE) {
                            Hawk.put("text", data);
                        }
                    } else {
                        data = new ArrayList<String>();
                        data.add(wrd);
                        if(binding.savedHide.getVisibility() != View.VISIBLE) {
                            Hawk.put("text", data);
                        }
                    }
                    if (binding.savedHide.getVisibility() != View.VISIBLE) {
                        binding.savedHide.setVisibility(View.VISIBLE);
                    } else {
//                        data = new ArrayList<String>();
                        data = Hawk.get("text");
                        data.remove(wrd);

                        if(binding.savedHide.getVisibility() == View.VISIBLE) {
                            Hawk.put("text", data);
                        }


                        binding.savedHide.setVisibility(View.GONE);
                    }
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(WordDetailsActivity.this);
                    builder.setMessage("Please Login First For Save Word!")
                            .setCancelable(false)
                            .setPositiveButton("Login", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(WordDetailsActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                  //  finishAffinity();
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.setTitle("Save Word");
                    alert.show();
                }
            }
        });
    }

    public void onBackPressed() {
        tts.shutdown();
        super.onBackPressed();
    }

    public void speaker(String s, String word) {
        tts.speak(finalResponseData, TextToSpeech.QUEUE_FLUSH, null);
        if (s.equals("speak")) {
            binding.volume.setBackgroundResource(R.drawable.volume_background);
        } else {
            binding.volume.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    public void sharetextOnly(String title) {
        String sharebody = title;
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, sharebody);
        startActivity(Intent.createChooser(intent, "Share Via"));
    }

    public void definitionApi() {
        apiEndPoint = ServiceGenerator.createService(ApiEndPoint.class);
        String accessToken = "q095std75p9uysknwxmaxeyw0zqzsaa0spj8jln0rm9tiv653";
        String wrd = getIntent().getStringExtra("word");

        String def = "v4/word.json/" + wrd + "/definitions";
        definitionResponseModels = apiEndPoint.getDefinition(def, accessToken);
        definitionResponseModels.enqueue(new Callback<List<DefinitionResponseModel>>() {
            @Override
            public void onResponse(Call<List<DefinitionResponseModel>> call, Response<List<DefinitionResponseModel>> response) {
                if (response.isSuccessful()) {

                    binding.definitionRv.setLayoutManager(new LinearLayoutManager(getApplication()));
                    List<String> definition = new ArrayList();
                    for (int i = 0; i < response.body().size(); i++) {

                        if ((response.body().get(i).getText()) != null) {
                            definition.add(String.valueOf(Html.fromHtml(response.body().get(i).getText())));

                        } else {
                            definition.add("");
                            Toast.makeText(WordDetailsActivity.this, "Something Went Wrong!", Toast.LENGTH_SHORT).show();
                        }
                        binding.definitionsCv.setVisibility(View.VISIBLE);
                        finalResponseData = new StringBuilder().append(finalResponseData).append(definition).toString();
                    }
                    binding.definitionRv.setAdapter(new ExampleAdapter(definition));
                }
            }

            @Override
            public void onFailure(Call<List<DefinitionResponseModel>> call, Throwable t) {


            }
        });
    }

    public void exampleApi() {
        apiEndPoint = ServiceGenerator.createService(ApiEndPoint.class);
        String accessToken = "q095std75p9uysknwxmaxeyw0zqzsaa0spj8jln0rm9tiv653";

        String wrd = getIntent().getStringExtra("word");
        String examplee = "https://api.wordnik.com/v4/word.json/" + wrd + "/examples";

        exampleResponseModels = apiEndPoint.getExample(examplee, accessToken);
        exampleResponseModels.enqueue(new Callback<ExampleResponseModel>() {

            @Override
            public void onResponse(Call<ExampleResponseModel> call, Response<ExampleResponseModel> response) {
                if (response.isSuccessful()) {
                    binding.examplesRv.setLayoutManager(new LinearLayoutManager(getApplication()));
                    List<String> example = new ArrayList();
                    for (int i = 0; i < response.body().getExamples().size(); i++) {

                        if ((response.body().getExamples().get(i).getText()) != null) {
                            example.add(String.valueOf(Html.fromHtml(response.body().getExamples().get(i).getText())));
                        } else {
                            example.add("");
                            Toast.makeText(WordDetailsActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                        binding.exampleCv.setVisibility(View.VISIBLE);
                        finalResponseData = new StringBuilder().append(finalResponseData).append(response.body().getExamples().get(i).getText()).toString();
                    }
                    binding.examplesRv.setAdapter(new ExampleAdapter(example));

                    binding.share.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            view.setEnabled(false);
                            sharetextOnly("Word-  " + wrd + "\n\nExample- " + response.body().getExamples().get(0).getText());

//                            Handler handler = new Handler();
//                            handler.postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    view.setEnabled(true);
//                                }
//                            }, 2000);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ExampleResponseModel> call, Throwable t) {

            }
        });
    }
}