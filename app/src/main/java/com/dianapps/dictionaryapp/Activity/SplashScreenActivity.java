package com.dianapps.dictionaryapp.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.dianapps.dictionaryapp.R;
import com.dianapps.dictionaryapp.databinding.SplashLayoutBinding;
import com.orhanobut.hawk.Hawk;

@SuppressLint("CustomSplashScreen")
public class SplashScreenActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        SplashLayoutBinding binding= DataBindingUtil.setContentView(this,R.layout.splash_layout);
        Hawk.init(this).build();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String token = Hawk.get("token");
                Intent intent;
                if(token == null || token.equals("")) {
                    intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                }else{
                    intent = new Intent(SplashScreenActivity.this, HomeActivity.class);
                }
                startActivity(intent);
                finish();
            }
        },2000);
    }
}