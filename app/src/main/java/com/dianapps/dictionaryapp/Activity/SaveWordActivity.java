package com.dianapps.dictionaryapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.Toast;

import com.dianapps.dictionaryapp.Adapter.HistoryAdapter;
import com.dianapps.dictionaryapp.R;
import com.dianapps.dictionaryapp.databinding.ActivitySaveWordBinding;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SaveWordActivity extends AppCompatActivity {

    HistoryAdapter adapter;
    @SuppressLint("NotifyDataSetChanged")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Hawk.init(this).build();
        ActivitySaveWordBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_save_word);
        binding.back.setOnClickListener(view -> onBackPressed());
        binding.saveRv.setLayoutManager(new LinearLayoutManager(this));

//       String hSize= Hawk.get("text");
//       hSize.length();
//       List<String> list = new ArrayList<>();
//        list.add("Empty Save Word");
//        String list =Hawk.get("text");

        if (Hawk.get("text") != null) {
            adapter = new HistoryAdapter(Hawk.get("text"), SaveWordActivity.this);
        } else {
            Toast.makeText(this, "No Save Word Found!", Toast.LENGTH_SHORT).show();
        }
        binding.saveRv.setAdapter(adapter);
        // Collections.reverse(list);
    }
}